navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

const modelParams = {
    flipHorizontal: true, // flip e.g for video
    imageScaleFactor: 0.7, // reduce input image size for gains in speed.
    maxNumBoxes: 20, // maximum number of boxes to detect
    iouThreshold: 0.5, // ioU threshold for non-max suppression
    scoreThreshold: 0.79 // confidence threshold for predictions.
  };

const canvas = document.querySelector("#canvas");
const context = canvas.getContext("2d");
const video = document.querySelector("#video");
let updateNote = document.querySelector("#updatenote");

let model = null;

handTrack.startVideo(video).then(status => {
  if (status) {
    navigator.getUserMedia(
      { video: {} },
      stream => {
        video.srcObject = stream;
        setInterval(runDetection, 100);
      },
      error => console.log(error)
    );
  }
});

function runDetection() {
  model.detect(video).then(predictions => {
    console.log("Predictions: ", predictions);
    model.renderPredictions(predictions, canvas, context, video);
    requestAnimationFrame(runDetection);
    if(predictions.length > 0){
        //Do something is has prediction
    }
  });
}

handTrack.load(modelParams).then(lmodel => {
  model = lmodel;
});


